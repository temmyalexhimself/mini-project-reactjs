import http from "../http-common";

const getAll = () => {
  return http.get("/item-sales");
};

const get = id => {
  return http.get(`/item-sales/${id}`);
};

const create = data => {
  return http.post("/item-sales", data);
};

const update = (id, data) => {
  return http.put(`/item-sales/${id}`, data);
};

const remove = id => {
  return http.delete(`/item-sales/${id}`);
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
};
