import http from "../http-common";

const getAll = () => {
  return http.get("/sales");
};

const get = id => {
  return http.get(`/sales/${id}`);
};

const create = data => {
  return http.post("/sales", data);
};

const update = (id, data) => {
  return http.put(`/sales/${id}`, data);
};

const remove = id => {
  return http.delete(`/sales/${id}`);
};

const removeAll = () => {
  return http.delete(`/sales`);
};

const findByTitle = title => {
  return http.get(`/sales?title=${title}`);
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByTitle
};
