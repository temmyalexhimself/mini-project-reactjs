import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import AddCustomer from "./components/customers/AddCustomer";
import Customer from "./components/customers/Customer";
import CustomersList from "./components/customers/CustomersList";

import AddItem from "./components/items/AddItem";
import Item from "./components/items/Item";
import ItemsList from "./components/items/ItemsList";

import AddSales from "./components/sales/AddSales";
import Sales from "./components/sales/Sales";
import SalesList from "./components/sales/SalesList";

import AddItemSales from "./components/item-sales/AddItemSales";
import ItemSales from "./components/item-sales/ItemSales";
import ItemSalesList from "./components/item-sales/ItemSalesList";

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="#" className="navbar-brand">
          CRUD APP
        </a>
        <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/customers"} className="nav-link">
                Pelanggan
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/items"} className="nav-link">
                Barang
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/item-sales"} className="nav-link">
                Item Penjualan
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/sales"} className="nav-link">
                Penjualan
              </Link>
            </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path="/customers" component={CustomersList} />
          <Route exact path="/add-customer" component={AddCustomer} />
          <Route path="/customers/:id" component={Customer} />

          <Route exact path="/items" component={ItemsList} />
          <Route exact path="/add-item" component={AddItem} />
          <Route path="/items/:id" component={Item} />

          <Route exact path="/sales" component={SalesList} />
          <Route exact path="/add-sales" component={AddSales} />
          <Route path="/sales/:id" component={Sales} />

          <Route exact path="/item-sales" component={ItemSalesList} />
          <Route exact path="/add/item-sales" component={AddItemSales} />
          <Route path="/item-sales/:id" component={ItemSales} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
