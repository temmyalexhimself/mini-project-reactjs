import axios from "axios";

// export default axios.create({
//   baseURL: "http://localhost:8080/api",
//   headers: {
//     "Content-type": "application/json"
//   }
// });


// export default axios.create({
//   baseURL: "https://mini-project.perdosrijaya.org/api/v1",
//   headers: {
//     "Content-type": "application/json"
//   }
// });

export default axios.create({
  baseURL: "http://mini-project.test/api/v1",
  headers: {
    "Content-type": "application/json"
  }
});
