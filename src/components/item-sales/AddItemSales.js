import React, { useEffect, useState } from "react";
import ItemSalesDataService from "../../services/ItemSalesService";
import ItemDataService from "../../services/ItemService";

const AddItemSales = () => {
  const initialItemSalesState = {
    id: null,
    item_id: "",
    qty: "",
  };
  const [itemsales, setItemSales] = useState(initialItemSalesState);
  const [submitted, setSubmitted] = useState(false);
  const [items, setItems] = useState([]);
  const [validation, setValidation] = useState([]);

  useEffect(() => {
    retrieveItems();
  }, []);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setItemSales({ ...itemsales, [name]: value });
  };

  const retrieveItems = () => {
    ItemDataService.getAll()
      .then(response => {
        setItems(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const saveItemSales = () => {
    var data = {
      item_id: itemsales.item_id,
      qty: itemsales.qty,
    };

    ItemSalesDataService.create(data)
      .then(response => {
        setItemSales({
          id: response.data.id,
          item_id: response.data.item_id,
          qty: response.data.qty,
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((error) => {
        setValidation(error.response.data.error);
        // console.log(error.response.data);
      });
  };

  const newItemSales = () => {
    setItemSales(initialItemSalesState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newItemSales}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <h4>Item Sales</h4>
          <div className="form-group">
            <label htmlFor="item_id">Item</label>
            <select name="item_id" id="item_id" className="form-control" onChange={handleInputChange}>
            <option value="">Select Item</option>
            {items &&
              items.map((item, index) => ( 
                <option value={item.id} key={item.id}>{item.name}</option>
              ))}
            </select>
            {
                validation.item_id && (
                  <div className="text-danger">
                    <small>{validation.item_id}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="qty">Qty</label>
            <input
              type="number"
              className="form-control"
              id="qty"
              required
              value={itemsales.qty}
              onChange={handleInputChange}
              name="qty"
            />
            {
                validation.qty && (
                  <div className="text-danger">
                    <small>{validation.qty}</small>
                  </div>
                )
            }
          </div>

          <button onClick={saveItemSales} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};

export default AddItemSales;
