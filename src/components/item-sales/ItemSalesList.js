import React, { useState, useEffect } from "react";
import ItemSalesDataService from "../../services/ItemSalesService";
import { Link } from "react-router-dom";

const ItemSalesList = () => {
  const [itemsaleses, setItemSales] = useState([]);
  const [currentItemSales, setCurrentItemSales] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);

  useEffect(() => {
    retrieveItemSales();
  }, []);

  const retrieveItemSales = () => {
    ItemSalesDataService.getAll()
      .then(response => {
        setItemSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const setActiveItemSales = (customer, index) => {
    setCurrentItemSales(customer);
    setCurrentIndex(index);
  };

  return (
    <div className="list row">
      <div className="col-md-12">
        <a href="/add/item-sales" className="btn btn-success mb-4">Tambah</a>
        <h4 className="mb-4">List Item Penjualan</h4>
        <ul className="list-group">
          {itemsaleses &&
            itemsaleses.map((item_sales, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveItemSales(item_sales, index)}
                key={index}
              >
                {item_sales.item_name}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-md-6">
        {currentItemSales ? (
          <div>
            <h4 className="mt-4">Item Penjualan</h4>
            <div>
              <label>
                <strong>Barang:</strong>
              </label>{" "}
              {currentItemSales.item_name}
            </div>
            <div>
              <label>
                <strong>QTY:</strong>
              </label>{" "}
              {currentItemSales.qty}
            </div>

            <Link
              to={"/item-sales/" + currentItemSales.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Item Sales...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ItemSalesList;
