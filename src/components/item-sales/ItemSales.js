import React, { useState, useEffect } from "react";
import ItemSalesDataService from "../../services/ItemSalesService";
import ItemDataService from "../../services/ItemService";

const ItemSales = props => {
  const initialItemSalesState = {
    id: null,
    item_id: 0,
    qty: 0,
  };
  const [currentItemSales, setCurrentItemSales] = useState(initialItemSalesState);
  const [message, setMessage] = useState("");
  const [items, setItems] = useState([]);

  const getItemSales = id => {
    ItemSalesDataService.get(id)
      .then(response => {
        setCurrentItemSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getItemSales(props.match.params.id);
  }, [props.match.params.id]);

  useEffect(() => {
    retrieveItems();
  }, []);

  const retrieveItems = () => {
    ItemDataService.getAll()
      .then(response => {
        setItems(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentItemSales({ ...currentItemSales, [name]: value });
  };

  const updateItemSales = () => {
    ItemSalesDataService.update(currentItemSales.id, currentItemSales)
      .then(response => {
        console.log(response.data);
        setMessage("The Item Sales was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteItemSales = () => {
    ItemSalesDataService.remove(currentItemSales.id)
      .then(response => {
        console.log(response.data);
        props.history.push("/item-sales");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentItemSales ? (
        <div className="edit-form">
          <h4>Item Sales</h4>
          <form>
            <div className="form-group">
              <label htmlFor="item_id">Item</label>
              <select name="item_id" id="item_id" className="form-control" value={currentItemSales.item_id} onChange={handleInputChange}>
              <option value="">Select Item</option>
              {items &&
                items.map((item, index) => ( 
                  <option value={item.id} key={item.id}>{item.name}</option>
                ))}
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="qty">Qty</label>
              <input
                type="text"
                className="form-control"
                id="qty"
                name="qty"
                value={currentItemSales.qty}
                onChange={handleInputChange}
              />
            </div>
          </form>

          <button className="badge badge-danger mr-2" onClick={deleteItemSales}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateItemSales}
          >
            Update
          </button>
          <p className="mt-4">{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a Customer...</p>
        </div>
      )}
    </div>
  );
};

export default ItemSales;
