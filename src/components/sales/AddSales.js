import React, { useEffect, useState } from "react";
import SalesDataService from "../../services/SalesService";
import CustomerDataService from "../../services/CustomerService";
import ItemSalesDataService from "../../services/ItemSalesService";

const AddSales = () => {
  const initialSalesState = {
    id: null,
    date: "",
    customer_id: "",
    item_sales_id: ""
  };
  const [sales, setSales] = useState(initialSalesState);
  const [submitted, setSubmitted] = useState(false);
  const [customers, setCustomers] = useState([]);
  const [itemsaleses, setItemSales] = useState([]);
  const [validation, setValidation] = useState([]);

  useEffect(() => {
    retrieveCustomers();
    retrieveItemSales();
  }, []);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setSales({ ...sales, [name]: value });
  };

  const retrieveCustomers = () => {
    CustomerDataService.getAll()
      .then(response => {
        setCustomers(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveItemSales = () => {
    ItemSalesDataService.getAll()
      .then(response => {
        setItemSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const saveSales = () => {
    var data = {
      date: sales.date,
      customer_id: sales.customer_id,
      item_sales_id: sales.item_sales_id
    };

    SalesDataService.create(data)
      .then(response => {
        setSales({
          id: response.data.id,
          date: response.data.date,
          customer_id: response.data.customer_id,
          item_sales_id: response.data.item_sales_id
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((error) => {
        setValidation(error.response.data.error);
      });
  };

  const newSales = () => {
    setSales(initialSalesState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newSales}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="date">Date</label>
            <input
              type="date"
              className="form-control"
              id="date"
              required
              value={sales.date}
              onChange={handleInputChange}
              name="date"
            />
            {
                validation.date && (
                  <div className="text-danger">
                    <small>{validation.date}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="customer_id">Customer</label>
            <select name="customer_id" id="customer_id" className="form-control" onChange={handleInputChange}>
            <option value="">Select Customer</option>
            {customers &&
              customers.map((customer, index) => ( 
                <option value={customer.id} key={customer.id}>{customer.name}</option>
              ))}
            </select>
            {
                validation.customer_id && (
                  <div className="text-danger">
                    <small>{validation.customer_id}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="item_sales_id">Nota</label>
            <select name="item_sales_id" id="item_sales_id" className="form-control" onChange={handleInputChange}>
            <option value="">Select Nota</option>
            {itemsaleses &&
              itemsaleses.map((item_sales, index) => ( 
                <option value={item_sales.id} key={item_sales.id}>{item_sales.id}</option>
              ))}
            </select>
            {
                validation.item_sales_id && (
                  <div className="text-danger">
                    <small>{validation.item_sales_id}</small>
                  </div>
                )
            }
          </div>

          <button onClick={saveSales} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};

export default AddSales;
