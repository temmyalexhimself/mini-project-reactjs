import React, { useState, useEffect } from "react";
import SalesDataService from "../../services/SalesService";
import CustomerDataService from "../../services/CustomerService";
import ItemSalesDataService from "../../services/ItemSalesService";

const Sales = props => {
  const initialSalesState = {
    id: null,
    date: "",
    customer_id: 0,
    item_sales_id: 0
  };
  const [currentSales, setCurrentSales] = useState(initialSalesState);
  const [message, setMessage] = useState("");
  const [customers, setCustomers] = useState([]);
  const [itemsaleses, setItemSales] = useState([]);

  const getSales = id => {
    SalesDataService.get(id)
      .then(response => {
        setCurrentSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };
  

  useEffect(() => {
    getSales(props.match.params.id);
  }, [props.match.params.id]);

  useEffect(() => {
    retrieveCustomers();
    retrieveItemSales();
  }, []);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentSales({ ...currentSales, [name]: value });
  };

  const retrieveCustomers = () => {
    CustomerDataService.getAll()
      .then(response => {
        setCustomers(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const retrieveItemSales = () => {
    ItemSalesDataService.getAll()
      .then(response => {
        setItemSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const updateSales = () => {
    SalesDataService.update(currentSales.id, currentSales)
      .then(response => {
        console.log(response.data);
        setMessage("The Sales was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteSales = () => {
    SalesDataService.remove(currentSales.id)
      .then(response => {
        console.log(response.data);
        props.history.push("/sales");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentSales ? (
        <div className="edit-form">
          <h4>Sales</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Date</label>
              <input
                type="date"
                className="form-control"
                id="date"
                name="date"
                value={currentSales.date}
                onChange={handleInputChange}
              />
            </div>
            

            <div className="form-group">
              <label htmlFor="customer_id">Customer</label>
              <select name="customer_id" id="customer_id" className="form-control" value={currentSales.customer_id} onChange={handleInputChange}>
              <option value="">Select Customer</option>
              {customers &&
                customers.map((customer, index) => ( 
                  <option value={customer.id} key={customer.id}>{customer.name}</option>
                ))}
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="item_sales_id">Nota</label>
              <select name="item_sales_id" id="item_sales_id" className="form-control" value={currentSales.item_sales_id} onChange={handleInputChange}>
              <option value="">Select Nota</option>
              {itemsaleses &&
                itemsaleses.map((itemsales, index) => ( 
                  <option value={itemsales.id} key={itemsales.id}>{itemsales.id}</option>
                ))}
              </select>
            </div>
          </form>

          <button className="badge badge-danger mr-2" onClick={deleteSales}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateSales}
          >
            Update
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a Item...</p>
        </div>
      )}
    </div>
  );
};

export default Sales;
