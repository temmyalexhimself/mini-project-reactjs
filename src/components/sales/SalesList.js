import React, { useState, useEffect } from "react";
import SalesDataService from "../../services/SalesService";
import { Link } from "react-router-dom";

const SalesList = () => {
  const [datasales, setSales] = useState([]);
  const [currentSales, setCurrentSales] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);

  useEffect(() => {
    retrieveSales();
  }, []);

  const retrieveSales = () => {
    SalesDataService.getAll()
      .then(response => {
        setSales(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const setActiveItem = (customer, index) => {
    setCurrentSales(customer);
    setCurrentIndex(index);
  };

  return (
    <div className="list row">
      <div className="col-md-12">
        <a href="/add-sales" className="btn btn-success mb-4">Tambah</a>
        <h4 className="mb-4">List Penjualan</h4>
        <ul className="list-group">
          {datasales &&
            datasales.map((sales, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveItem(sales, index)}
                key={index}
              >
                {sales.indo_date} - {sales.customer_name} - {sales.subtotal}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-md-6">
        {currentSales ? (
          <div>
            <h4 className="mt-4">Sales</h4>
            <div>
              <label>
                <strong>Date:</strong>
              </label>{" "}
              {currentSales.date}
            </div>
            <div>
              <label>
                <strong>Customer:</strong>
              </label>{" "}
              {currentSales.customer_name}
            </div>
            <div>
              <label>
                <strong>Subtotal:</strong>
              </label>{" "}
              {currentSales.subtotal}
            </div>

            <Link
              to={"/sales/" + currentSales.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Sales...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default SalesList;
