import React, { useState } from "react";
import ItemDataService from "../../services/ItemService";

const AddItem = () => {
  const initialItemState = {
    id: null,
    name: "",
    category: "",
    price: ""
  };
  const [item, setItem] = useState(initialItemState);
  const [submitted, setSubmitted] = useState(false);
  const [validation, setValidation] = useState([]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setItem({ ...item, [name]: value });
  };

  const saveItem = () => {
    var data = {
      name: item.name,
      category: item.category,
      price: item.price
    };

    ItemDataService.create(data)
      .then(response => {
        setItem({
          id: response.data.id,
          name: response.data.name,
          category: response.data.category,
          price: response.data.price
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((error) => {
        setValidation(error.response.data.error);
        console.log(error.response.data.error);
      });
  };

  const newItem = () => {
    setItem(initialItemState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newItem}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              required
              value={item.name}
              onChange={handleInputChange}
              name="name"
            />
            {
                validation.name && (
                  <div className="text-danger">
                    <small>{validation.name}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="category">Category</label>
            <input
              type="text"
              className="form-control"
              id="category"
              required
              value={item.category}
              onChange={handleInputChange}
              name="category"
            />
            {
                validation.name && (
                  <div className="text-danger">
                    <small>{validation.category}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control"
              id="price"
              required
              value={item.price}
              onChange={handleInputChange}
              name="price"
            />
            {
                validation.price && (
                  <div className="text-danger">
                    <small>{validation.price}</small>
                  </div>
                )
            }
          </div>

          <button onClick={saveItem} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};

export default AddItem;
