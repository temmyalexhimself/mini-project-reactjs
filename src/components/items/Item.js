import React, { useState, useEffect } from "react";
import ItemDataService from "../../services/ItemService";

const Item = props => {
  const initialItemState = {
    id: null,
    name: "",
    domicile: "",
    gender: ""
  };
  const [currentItem, setCurrentItem] = useState(initialItemState);
  const [message, setMessage] = useState("");

  const getCustomer = id => {
    ItemDataService.get(id)
      .then(response => {
        setCurrentItem(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getCustomer(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentItem({ ...currentItem, [name]: value });
  };

  const updateItem = () => {
    ItemDataService.update(currentItem.id, currentItem)
      .then(response => {
        console.log(response.data);
        setMessage("The Item was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteItem = () => {
    ItemDataService.remove(currentItem.id)
      .then(response => {
        console.log(response.data);
        props.history.push("/items");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentItem ? (
        <div className="edit-form">
          <h4>Items</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={currentItem.name}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="category">Category</label>
              <input
                type="text"
                className="form-control"
                id="category"
                name="category"
                value={currentItem.category}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Price:</strong>
              </label>
              <input
                type="text"
                className="form-control"
                id="category"
                name="category"
                value={currentItem.price}
                onChange={handleInputChange}
              />
            </div>
          </form>

          <button className="badge badge-danger mr-2" onClick={deleteItem}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateItem}
          >
            Update
          </button>
          <p className="mt-4">{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a Item...</p>
        </div>
      )}
    </div>
  );
};

export default Item;
