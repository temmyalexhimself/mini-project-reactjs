import React, { useState, useEffect } from "react";
import ItemDataService from "../../services/ItemService";
import { Link } from "react-router-dom";

const ItemsList = () => {
  const [items, setItems] = useState([]);
  const [currentItem, setCurrentItem] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);

  useEffect(() => {
    retrieveItems();
  }, []);

  const retrieveItems = () => {
    ItemDataService.getAll()
      .then(response => {
        setItems(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const setActiveItem = (item, index) => {
    setCurrentItem(item);
    setCurrentIndex(index);
  };

  return (
    <div className="list row">
      <div className="col-md-6">
        <a href="/add-item" className="btn btn-success mb-4">Tambah</a>
        <h4 className="mb-4">List Barang</h4>
        <ul className="list-group">
          {items &&
            items.map((item, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveItem(item, index)}
                key={index}
              >
                {item.name}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-md-6">
        {currentItem ? (
          <div>
            <h4 className="mt-4">Item</h4>
            <div>
              <label>
                <strong>Name:</strong>
              </label>{" "}
              {currentItem.name}
            </div>
            <div>
              <label>
                <strong>Category:</strong>
              </label>{" "}
              {currentItem.category}
            </div>
            <div>
              <label>
                <strong>Price:</strong>
              </label>{" "}
              {currentItem.price}
            </div>

            <Link
              to={"/items/" + currentItem.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Items...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ItemsList;
