import React, { useState } from "react";
import CustomerDataService from "../../services/CustomerService";

const AddCustomer = () => {
  const initialCustomerState = {
    id: null,
    name: "",
    domicile: "",
    gender: ""
  };
  const [customer, setCustomer] = useState(initialCustomerState);
  const [submitted, setSubmitted] = useState(false);
  const [validation, setValidation] = useState([]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCustomer({ ...customer, [name]: value });
  };

  const saveCustomer = () => {
    var data = {
      name: customer.name,
      domicile: customer.domicile,
      gender: customer.gender
    };

    CustomerDataService.create(data)
      .then(response => {
        setCustomer({
          id: response.data.id,
          name: response.data.name,
          domicile: response.data.domicile,
          gender: response.data.gender
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((error) => {
        setValidation(error.response.data.error);
      });
  };

  const newCustomer = () => {
    setCustomer(initialCustomerState);
    setSubmitted(false);
  };

  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newCustomer}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              required
              value={customer.name}
              onChange={handleInputChange}
              name="name"
            />
            {
                validation.name && (
                  <div className="text-danger">
                    <small>{validation.name}</small>
                  </div>
                )
            }
          </div>
         

          <div className="form-group">
            <label htmlFor="domicile">Domisili</label>
            <input
              type="text"
              className="form-control"
              id="domicile"
              required
              value={customer.domicile}
              onChange={handleInputChange}
              name="domicile"
            />
            {
                validation.domicile && (
                  <div className="text-danger">
                    <small>{validation.domicile}</small>
                  </div>
                )
            }
          </div>

          <div className="form-group">
            <label htmlFor="gender">Jenis Kelamin</label>
            <select name="gender" id="gender" className="form-control" onChange={handleInputChange} value={customer.gender}>
              <option value="">Pilih Jenis Kelamin</option>
              <option value="Pria">Pria</option>
              <option value="Wanita">Wanita</option>
            </select>
            {
                validation.gender && (
                    <div className="text-danger">
                      <small>{validation.gender}</small>
                    </div>
                )
            }
          </div>

          <button onClick={saveCustomer} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};

export default AddCustomer;
