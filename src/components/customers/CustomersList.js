import React, { useState, useEffect } from "react";
import CustomerDataService from "../../services/CustomerService";
import { Link } from "react-router-dom";

const CustomersList = () => {
  const [customers, setCustomers] = useState([]);
  const [currentCustomer, setCurrentCustomer] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);

  useEffect(() => {
    retrieveCustomers();
  }, []);

  const retrieveCustomers = () => {
    CustomerDataService.getAll()
      .then(response => {
        setCustomers(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const setActiveCustomer = (customer, index) => {
    setCurrentCustomer(customer);
    setCurrentIndex(index);
  };

  return (
    <div className="list row">
      <div className="col-md-12">
        <a href="/add-customer" className="btn btn-success mb-4">Tambah</a>
        <h4 className="mb-4">List Pelanggan</h4>
        <ul className="list-group">
          {customers &&
            customers.map((customer, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveCustomer(customer, index)}
                key={index}
              >
                {customer.name}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-md-6">
        {currentCustomer ? (
          <div>
            <h4>Customer</h4>
            <div>
              <label>
                <strong>Name:</strong>
              </label>{" "}
              {currentCustomer.name}
            </div>
            <div>
              <label>
                <strong>Domicile:</strong>
              </label>{" "}
              {currentCustomer.domicile}
            </div>
            <div>
              <label>
                <strong>Gender:</strong>
              </label>{" "}
              {currentCustomer.gender}
            </div>

            <Link
              to={"/customers/" + currentCustomer.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Customer...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default CustomersList;
